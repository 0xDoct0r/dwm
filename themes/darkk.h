static const char col_gray1[]       = "#110019";
static const char col_gray2[]       = "#000000";
static const char col_gray3[]       = "#777777";
static const char col_gray4[]       = "#FFFFFF";
static const char col_gray5[]       = "#a000ea";
static const char col_gray6[]       = "#44444b";
static const char col_gray7[]       = "#24415a";
